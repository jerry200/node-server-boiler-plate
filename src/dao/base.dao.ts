import AppConfigLoader from '../../config/app/app-config-loader';
import Logger from '../logger/logger';
import Knex from 'knex';

const logger = Logger.getInstance();
const appConfigLoaderObj = new AppConfigLoader();
const mysqlProperties = appConfigLoaderObj.getMysqlProperties();

const configuration: Knex.Config = {
  client: 'mysql',
  connection: {
    host: mysqlProperties.host,
    port: mysqlProperties.portNo,
    user: mysqlProperties.user,
    password: mysqlProperties.password,
    database: mysqlProperties.dbName
  },
  pool: {
    min: 0,
    max: 7
  }
};

export class Connection {
  public knex(): Knex {
      return Knex(configuration);
  }
}
