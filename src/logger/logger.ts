import winston = require('winston');

import AppConfigLoader from '../../config/app/app-config-loader';

export default class Logger {
	
	private static instance: Logger;
	private static Level = {ERROR:1, WARN:2, INFO:3, DEBUG:4};	
	private static LEVEL: any;
	private DIRECTORY!: string;	

	private logger: winston.LoggerInstance;

	static getInstance() {
		return this.instance || (this.instance = new this());
	}

	private constructor() {
		this.initialize();
	}
	
	private initialize() {
		let logConfig = AppConfigLoader.getInstance().getLogProperties();
		
		// log level
		switch (logConfig.level) {
			case 'error': Logger.LEVEL = Logger.Level.ERROR; break;
			case 'warn': Logger.LEVEL = Logger.Level.WARN; break;
			case 'info': Logger.LEVEL = Logger.Level.INFO; break;
			case 'debug': Logger.LEVEL = Logger.Level.DEBUG; break;
			default: Logger.LEVEL = Logger.Level.INFO; break;						
		}
		
		// log directory
		this.DIRECTORY = logConfig.directory;
		
		this.logger = new (winston.loggers)({
			transports: [
				new (winston.transports.Console)(),
				new (winston.transports.File)({name: 'all-file', filename: this.DIRECTORY + '/server.log'}),
				new (winston.transports.File)({name: 'error-file', level: 'error', filename: this.DIRECTORY + '/server.error.log'})
			]
		});	
	}
	
	public error(msg: string): void {		
		if( Logger.LEVEL >= Logger.Level.ERROR ) {
			this.logger.error(msg);
		}
	}

	public warn(msg: string): void {		
		if( Logger.LEVEL >= Logger.Level.WARN ) {
			this.logger.warn(msg);
		}
	}

	public info(msg: string): void {		
		if( Logger.LEVEL >= Logger.Level.INFO ) {
			this.logger.info(msg);
		}
	}

	public debug(msg: string): void {		
		if( Logger.LEVEL >= Logger.Level.DEBUG ) {
			this.logger.debug(msg);
		}
	}
	
}
