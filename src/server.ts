import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
import moment = require('moment');

class Server {
    public expressApp = express();
    public router = express.Router();

    constructor() {

        this.config();
        this.routes();
    }

    public config(): void {

        // express middleware
        this.expressApp.use(bodyParser.urlencoded({ extended: true, limit: '15mb' }));
        this.expressApp.use(bodyParser.json({ limit: '50mb' }));
    
        this.expressApp.use(compression());
        this.expressApp.use(helmet());
    
        this.expressApp.use((request, response, next) => {
          response.header('Access-Control-Allow-Origin', '*');
          response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
          next();
        });
    }

    public routes(): void {
        this.expressApp.get('/' , (req : Request , res : Response) => {
            return res.status(200).json({
              okay: 'true'
            });
          });

        this.expressApp.get('/status', (req : Request , res : Response) => {
        return res.status(200).json({
            version: process.env.VERSION,
            deployed: moment().format()
        });
        })
    }
}

export default new Server().expressApp;