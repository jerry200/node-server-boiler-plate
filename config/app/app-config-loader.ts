
const env = process.env.NODE_ENV || 'local';

import prodConfig = require('./app-prod.config.json');
import testConfig = require('./app-test.config.json');
import localConfig = require('./app-local.config.json');
import devConfig = require('./app-dev.config.json');
import demoConfig = require('./app-demo.config.json');

/**
 * Class to load the application configuration properties based on the enviroments
 */
export default class AppConfigLoader {

	private static instance: AppConfigLoader;

	private config: any;
	private applicationProperties: any;
	private logProperties: any;
	private emailProperties: any;
	private mysqlProperties: any;
	private awsProperties: any;
	constructor() {
		switch (env) {
			case 'development': { this.config = devConfig; }; break;
			case 'test': { this.config = testConfig; }; break;
			case 'prod': { this.config = prodConfig; }; break;
			case 'demo': { this.config = demoConfig; }; break;
			default: { this.config = demoConfig; }; break;
		}
		this.applicationProperties = this.config.application;
		this.logProperties = this.config.log;
		this.emailProperties = this.config.email;
		this.mysqlProperties = this.config.database;
		this.awsProperties = this.config.aws;
	}

	static getInstance() {
		return this.instance || (this.instance = new this());
	}

	public getEnvironment() {
		return env;
	}

	public getApplicationProperties() {
		return this.applicationProperties;
	}

	public getLogProperties() {
		return this.logProperties;
	}

	public getEmailingProperties() {
		return this.emailProperties;
	}

	public getMysqlProperties() {
		return this.mysqlProperties;
	}

	public getAWSProperties() {
		return this.awsProperties;
	}
}